# Webpack boilerplate

A webpack boilerplate package with Scss/EJS.

## Get Started

1. Install Node.
1. `npm install` to add packages.
1. `npm run dev` to start the development server.
1. `npm run build` to build the production assets to `dist` folder.
1. `npm run sprite` to create png sprite.
1. `npm run svgsprite` to create svg sprite.
1. `npm run styleguide` to create styleguide.

## Functions included

### Webpack

1. HMR
1. htmllint(.htmllintrc), stylelint(.stylelintrc), and eslint(.eslintrc) validators
1. Autoprefixer(.browserslistrc)
1. Image minification
1. Sass compile loader
1. EJS compile loader
1. Clean CSS loader (https://github.com/jakubpawlowicz/clean-css#constructor-options)
1. Omit unused CSS style

### NPM
1. SVG sprite
1. PNG sprite (normal & retina)
1. Styleguide generator


## Style

Bootstrap-reboot.scss is used as style reset.
(https://getbootstrap.com/docs/4.1/content/reboot/)
