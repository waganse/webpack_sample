const webpack = require('webpack');
const path = require('path');
const glob = require('glob');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// Custom hash
const hash = Date.now();

// ejs replacement
const tpl = {
  css: '<link rel="stylesheet" href="css/%s">',
  js: '<script src="js/%s"></script>'
};

// Entry points
let entries = {};
glob.sync("./src/js/**/[^_]*.js").map(function(file) {
  return entries[file.match(/\/([a-z]+)\.js/i)[1]] = file;
});

const webpackConfig = {
  entry: entries,
  output: {
    filename: 'js/[name]-bundle-'+ hash + '.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'eslint-loader',
            options: {
              configFile: '.eslintrc'
            }
          }
          // {
          //   loader: "babel-loader"
          // },
        ]
      },
      {
        test: /\.ejs$/,
        use: [
          {
            loader: 'htmllint-loader',
            query: {
              config: '.htmllintrc',
              failOnError: true,
              failOnWarning: false,
            }
          },
          {
            loader: 'html-loader',
            options: {
              // minimize: true
            }
          },
          {
            loader: 'ejs-html-loader'
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '../'
            }
          },
          { loader: 'css-loader' },
          { loader: 'postcss-loader' },
          {
            loader: 'clean-css-loader',
            options: {
              level: 2
            }
          },
          {
            loader: 'sass-loader',
            options: {
              includePaths: []
            }
          }
        ]
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 102400,
              name: '[name]-' + hash + '.[ext]',
              outputPath: 'img/'
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              pngquant: {
                quality: '80'
              }
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: 'src/img/*.svg',
        to: 'img/[name].svg'
      }
    ]),
    new HtmlReplaceWebpackPlugin([
      {
        pattern: /(<!--\s*|@@)(css|js):([\w-\/]+)(\s*-->)?/g,
        replacement: function(match, $1, type, file, $4, index, input) {
          let url = file + '-bundle-' + hash + '.' + type;

          // $1==='@@' <--EQ--> $4===undefined
          return $4 == undefined ? url : tpl[type].replace('%s', url)
        }
      }
    ]),
    new MiniCssExtractPlugin({
      filename: 'css/[name]-bundle-' + hash + '.css'
    }),
    new PurgecssPlugin({
      paths: glob.sync(path.resolve(__dirname, 'src/**/*'),  { nodir: true }),
    }),
    new StyleLintPlugin({
      configFile: '.stylelintrc'
    })
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          name: 'vendor',
          chunks: 'initial',
          enforce: true
        }
      }
    }
  }
};

// Multiple ejs entries
glob.sync("./src/**/[^_]*.ejs").map(function(filepath) {
  let filename = filepath.match(/\/([a-z]+)\.ejs/i)[1];

  webpackConfig.plugins.unshift(
    new HTMLWebpackPlugin({
      filename: filename + '.html',
      template: filepath,
      publicPath: './',
      inject: false
    })
  );
});

module.exports = webpackConfig;